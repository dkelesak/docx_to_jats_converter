#endnotes and footnotes text is accessed via document.endnotes and document.footnotes respectively
#endnotes and footnotes text does not appear when scanning the document (with iter_paragraphs())

#endnote position at the scanned document is detected via "----endnoteX----" string, where X: id of endnote
#footnote position at the scanned document is detected via "----footnoteX----" string, where X: id of footnote
#(the above strings are placed by doc2python library)

#endnotes and footnotes are treated as the same object(<fn>) by JATS XML
#the only difference is that endnotes are accompanied by the " fn-type="endnote" " attribute 

import re
from document_docx2python_library import get_document_by_docx2python_library_without_formatting, get_not_blank_paragraphs_without_formatting


#get list of endnotes' text
def get_endnotes_list():
    document = get_document_by_docx2python_library_without_formatting()
    #endnotes are at level 3 of document.endnotes
    endnotes_list = document.endnotes[0][0][0]

    #case there are endnotes
    if len(endnotes_list) > 2:
        #endnote texts appear from index 3 with step 2, the rest of contents are info inserted by docx2python
        endnotes_list = endnotes_list[3::2]
    #case there are not endnotes
    else:
        endnotes_list = []

    return endnotes_list


#get list of footnotes' text
def get_footnotes_list():
    document = get_document_by_docx2python_library_without_formatting()
    #endnotes are at level 3 of document.endnotes
    footnotes_list = document.footnotes[0][0][0]

    #case there are endnotes
    if len(footnotes_list) > 2:
        #endnote texts appear from index 3 with step 2, the rest of contents are info inserted by docx2python
        footnotes_list = footnotes_list[3::2]
    #case there are not endnotes
    else:
        footnotes_list = []

    return footnotes_list


#in case an endnote is found in this paragraph place the appropriate tags and endnote text at the right place


#get combined list of endnotes and footnotes in the order at which they appear at the text
def get_endnotes_footnotes_combined_list(): #(paragraph):

    #in general I use id when I start from 1 and index when I start from 0
    endnote_footnote_combined_id = 1
    endnotes_footnotes_combined_list = []

    paragraphs = get_not_blank_paragraphs_without_formatting()

    for paragraph in paragraphs:

        #case endnote
        found_endnote = re.search(r'----endnote+[\d+]+----', paragraph)
        if found_endnote:
            found_endnote_id = re.search(r'[\d+]', found_endnote.group())
            if found_endnote_id:
                #endnote_id starts from 1, endnote list index starts from 0
                endnote_id = int(found_endnote_id.group())
                endnote_index = endnote_id - 1
                endnote_text = get_endnotes_list()[endnote_index].strip()

                #fn-id follows always the combined endnote_footnote index
                #however label follows the separate endnote_index and not a combined one (this is how Word numbers them)
                endnote_str = "<fn fn-type=\"endnote\" id=\"fn-" + str(endnote_footnote_combined_id) + "\"><label>" + str(endnote_id) + "</label><p>" + endnote_text + "\"</p></fn>" 
                endnotes_footnotes_combined_list.append(endnote_str)
            endnote_footnote_combined_id += 1


        #case footnote
        found_footnote = re.search(r'----footnote+[\d+]+----', paragraph)
        if found_footnote:
            found_footnote_id = re.search(r'[\d+]', found_footnote.group())
            if found_footnote_id:
                #footnote_id starts from 1, footnote list index starts from 0
               
                footnote_id = int(found_footnote_id.group())
                footnote_index = footnote_id - 1
                footnote_text = get_footnotes_list()[footnote_index].strip()

                #fn-id follows always the combined footnote_footnote index
                #however label follows the separate footnote_index and not a combined one (this is how Word numbers them)
                footnote_str = "<fn id=\"fn-" + str(endnote_footnote_combined_id) + "\"><label>" + str(footnote_id) + "</label><p>" + footnote_text + "\"</p></fn>" 
                endnotes_footnotes_combined_list.append(footnote_str)
            endnote_footnote_combined_id += 1

    return endnotes_footnotes_combined_list





#run 
"""
endnotes_list = get_endnotes_list()
for item in endnotes_list:
    #remove empty space in beginning
    print(item.strip())
"""