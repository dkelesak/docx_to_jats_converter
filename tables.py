from docx2python.iterators import iter_rows
from document_docx2python_library import get_not_blank_paragraphs_without_formatting, get_document_by_docx2python_library_with_formatting, get_not_blank_paragraphs_with_formatting, fix_html
from tree import Tree

class Table:
    def __init__(self, table_index, caption, table_data):      
        self.table_index = table_index
        self.table_id = table_index + 1         #captions of tables inside the docx file start from 1 instead of 0
        self.caption = caption
        self.table_data = table_data


#scans the document and returns list of table objects 
def get_table_objects_list():

    document = get_document_by_docx2python_library_with_formatting()
    #now iter_rows instead of iter_paragraphs as usually
    rows = list(iter_rows(document.body))
    
    #fix html (adapt to JATS XML standard)
    rows_temp = [[]]
    for row in rows:                     #rows = [][], row = []
        row_temp = []
        for cell in row:
            cell = fix_html(cell)
            row_temp.append(cell)
        rows_temp.append(row_temp)
    rows = rows_temp

    #general table indexes
    table_objects_list = []
    table_index = 0
    
    #indexes related with the data within the table
    table_found = False
    table_data = []
    
    for row in rows:
        
        row_data = []

        #case: row with table contents
        if len(row) >= 2:
            table_found = True
            for cell in row:
                row_data.append(str(cell).strip("[]\'"))
            table_data.append(row_data)
        
        #case: end of table (e.g. row just after the end of table)
        elif len(row) == 1 and table_found == True:
            
            #create table object
            caption = possible_caption
            table = Table(table_index, caption, table_data)
            table_objects_list.append(table)
            table_index += 1

            #prepare variable for next iteration
            table_found = False
            table_data = []
            #caption is just before table     ??WHAT IF BLANK?
            possible_caption = str(row[-1][-1])

        #case: row without table contents
        elif len(row) == 1 and table_found == False:
        
            possible_caption = str(row[-1][-1])

    return table_objects_list 


#transforms table into a tree-format
def create_table_tree(table):

    table_tree = Tree("table-wrap", "", 0, ["id", "table-" + str(table.table_id)])
    table_tree.construct_add_child("label", "Table " + str(table.table_id))
    caption_node = table_tree.construct_add_child("caption", table.caption)

    #caption_node.construct_add_child("title", paragraph)                   #table caption
    alternatives_node = table_tree.construct_add_child("alternatives")

    #table data tree
    table_data_tree = Tree("table")
    tbody_node = table_data_tree.construct_add_child("tbody")    
    
    for row in table.table_data:
        row_tree = Tree("tr")
        for cell in row:     
            row_tree.construct_add_child("td", cell)
        tbody_node.add_child_object(row_tree)

    alternatives_node.add_child_object(table_data_tree)

    return table_tree

"""
table_number_check = re.search(r'\d+', table_check.group())

if table_number_check:
    table_number = table_number_check.group()
"""