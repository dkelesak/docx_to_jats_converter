#this class can be more useful for further extension of the program
#currently the focus is on classes: Book, Journal_Article, Online_Reference and Other_Reference, which inherit from Reference class 
class Reference:
  def __init__(self, reference_id, certainty):
    self.reference_id = reference_id
    self.certainty = certainty