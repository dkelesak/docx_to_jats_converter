import re 
from tree import Tree

#source: https://departments.weber.edu/qsupport&training/Data_Standards/Name.htm
prefix_list = ["1st Lt", "Adm", "Atty", "Brother", "Capt", "Chief", "Cmdr", "Col", "Dean", "Dr", "Elder", "Father", "Gen", "Gov", "Hon", "Lt Col", "Maj", "MSgt", "Mr", "Mrs", "Ms", "Prince", "Prof", "Rabbi", "Rev", "Sister"] 
suffix_list = ["II", "III", "IV", "CPA", "DDS", "Esq", "JD", "Jr", "LLD", "MD", "PhD", "Ret", "RN", "Sr", "DO"]


#takes a list of author dictionaries and creates the corresponding authors tree 
def create_authors_tree(authors_dictionary_list):

    authors_tree = Tree("person-group")    
    for author in authors_dictionary_list:
        author_tree = authors_tree.construct_add_child("name")
        #author's name attributes
        author_tree.construct_add_child("surname", author["surname"])
        author_tree.construct_add_child("given-names", author["given-names"])
        if author["prefix"] != "":
            author_tree.construct_add_child("prefix", author["prefix"])
        if author["suffix"] != "":
            author_tree.construct_add_child("suffix", author["suffix"])
    
    return authors_tree


#get list of author dictionaries (one dictionary for every author)
def get_authors_dictionaries(authors_str):

    authors_dictionary_list = []

    #split each author to a separate string
    author_str_list = split_authors(authors_str)
    #get dictionary of each author
    for author_str in author_str_list:
        author_dictionary = identify_components_author_str(author_str)
        #add author dictionary to dictionary list
        authors_dictionary_list.append(author_dictionary)

    return authors_dictionary_list


#split different authors e.g. split authors_str to a list of author_str
def split_authors(authors_str):

    #list of author strings (one string for every author)
    author_str_list = []

    #possible characters splitting different authors
    slash_split = authors_str.split("/")
    and_split = authors_str.split("&")

    #case 1: all authors split by "/"
    if len(slash_split) > 1:
        author_str_list = slash_split

    #case 2: all authors split by "&" (usually when authors are 2)
    elif len(and_split) > 1:
        initial_author_str_list = and_split

        for author_str in initial_author_str_list:
            comma_split = author_str.split(",")

            #case 3: authors split by "," and "&" (usually when authors are more than 2)
            if len(comma_split) > 1:
                temp_author_str_list = find_comma_split_names(author_str)
                for author_str in temp_author_str_list:
                    author_str_list.append(author_str)
            #if not case 3 then go on with case 2        
            else:
                author_str_list.append(author_str)

    #case 4: comma separated authors or 1 author
    else:                                        
        author_str_list = find_comma_split_names(authors_str)

    return author_str_list
    #return authors_dict_list

    
#this function works with comma split names and can recognise if they belong to the same author or not
#returns a list of strings, each of them corresponding to an author's full name
def find_comma_split_names(authors_str):
    
    author_str_list = []
    temp_name = ""
    author_str = "" #SOS IS IT NECESSARY?

    comma_split = authors_str.split(",")
    
    for comma_name in comma_split:
        one_letter_names = find_initials(comma_name)                           #find initials
        number_one_letter_names = len(one_letter_names)
            
        multi_letter_names = find_multi_letter_names(comma_name)               #find names written in full form
        number_multi_letter_names = len(multi_letter_names) 

        if number_one_letter_names + number_multi_letter_names >= 1:                      #check if it is a name     
            if number_one_letter_names + number_multi_letter_names >= 2 and number_multi_letter_names >= 1:          #format: surname1 given-name1, surname2 given-name2
                author_str = temp_name + comma_name 
                author_str_list.append(author_str)
                temp_name = ""
            elif temp_name != "":                                                                                    #format: surname1, given-name1, surname2, given-name2
                author_str = temp_name + comma_name + " " 
                author_str_list.append(author_str)
                temp_name = ""
            else:                                                                                                   #merge comma separated surname and given-name
                temp_name = comma_name

    #in case there is single name left in the end, add it to the last author name  
    if temp_name != "":
        #to keep their order
        temp_name_index = authors_str.find(temp_name)
        author_str_index = authors_str.find(author_str)
        #put back
        """
        if temp_name_index < author_str_index:
            author_str_list[-1] = temp_name + " " + author_str   
        else:
            author_str_list[-1] = author_str + " " + temp_name              
        """
    return author_str_list


#identify surname, given names, prefix, suffix
#input: single author string, output: author dictionary
def identify_components_author_str(author_str):

    surname_found = False

    prefix= ""
    surname = ""
    given_names = ""
    suffix = ""

    #split author_str word by word
    name_components = find_all_names(author_str)
    multi_letter_name_components = find_multi_letter_names(author_str)

    for name_component in name_components:
        
        name_component.strip()
        name_component.strip(".")    #for comparing with prefixes/suffixes - be careful not to lose data

        #case: prefix
        if name_component in prefix_list: 
            prefix = name_component
        #case suffix
        elif name_component in suffix_list:
            suffix = name_component
        #case surname (it is supposed the first multi-letter name is the surname)
        elif not surname_found and (name_component in multi_letter_name_components):
            surname = name_component
            surname_found = True
        #case given_names (it is supposed the multi-letter names other than the first are the given-names)
        else:
            given_names = given_names + " " + name_component

    author_dictionary = {"prefix": prefix, "surname": surname, "given-names": given_names, "suffix": suffix}       
    return author_dictionary


#returns list of multi-letter names, excluding prefixes and suffixes 
def find_multi_letter_names(name):
    multi_letter_names = re.findall(r'\b[A-Z][a-z]+\b', name)
    #remove prefixes anf suffixes
    multi_letter_names = [item for item in multi_letter_names if (item not in prefix_list and item not in suffix_list)]
    return multi_letter_names

#returns list of initials in name string
def find_initials(name):
    return re.findall(r'\b[A-Z]\b', name)                      

#returns list of all names either multi-letter names or initials
def find_all_names(name):                                                    #find multi letter names and initials
    return re.findall(r'\b[A-Z]\b|\b[A-Z][a-z]+\b', name)


#sos handle pavla -> tora kanei split me th pavla