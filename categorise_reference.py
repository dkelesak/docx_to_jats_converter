import re
from crossref import get_crossref_json_data, categorise_crossref_reference


#categorise reference based on the reference string and the corresponding list of 3 Reference_Sentence objects
#combinational algorithm: employs crossref, nltk and keyword search(like Vol.)
#output: "book"/"journal-article"/"online-reference/"other-reference"
#further development: check the similarity between the returned string from crossref and the reference string and adjust crossref's weight to the final decision
def categorise_reference(preprocessed_reference_str, sentence_objects_list):

    #STAGE 1: if online reference then: final desicion = online-reference
    #check if online reference
    online_reference_check = re.search(r'(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:/[^\s]*)?', preprocessed_reference_str) 
    if online_reference_check: 
        reference_category = "online-reference"
        return reference_category


    #stages 2, 3, 4 work cooperatively and desicion is taken by most points
    #calculate desicion points    
    decision_points_book = 0
    decision_points_journal_article = 0
    decision_points_other = 0

    #STAGE 2: keyword search. if keyword found then: increase points for corresponding category
    #add here book keywords
    book_keyword_check = re.search(r'ed\.|edition', preprocessed_reference_str, re.IGNORECASE)
    if book_keyword_check:   
        decision_points_book += 1

    #add here journal article keywords      
    journal_article_keyword_check = re.findall(r'Article|Issue|Journal|pages|pp(\.)?|Vol\.|Volume', preprocessed_reference_str)
    #case found 1 keyword (plus 1 point)
    if len(journal_article_keyword_check) == 1:   
        decision_points_journal_article += 1
    #case found more than 1 keywords (plus 1.5 points)
    elif len(journal_article_keyword_check) > 1:
        decision_points_journal_article += 1.5

    #STAGE 3: categorise with help of crossref: +1 points for returned category (crossref is not always accurate)
    #do similarity check of what I receive from crossref
    crossref_response_json = get_crossref_json_data(preprocessed_reference_str)
    reference_category_crossref = categorise_crossref_reference(crossref_response_json)
    if reference_category_crossref == "book":
        decision_points_book += 1
    elif reference_category_crossref == "journal-article":
        decision_points_journal_article += 1
    else:
        decision_points_other += 1

    #STAGE 4: take into account shallow textual features/syntactic     
    #appropriate sentence tokenization has been made so the last sentence corresponds to publisher information
    evaluating_sentence = sentence_objects_list[-1]
    #check proper nouns percentage = number of words starting with capital (e.g. NNP,NNPS) / number of character words
    proper_nouns_percentage = evaluating_sentence.get_proper_nouns_percentage()
    #check numbers percentage = number of numbers / (number of numbers + number of character words)
    numbers_percentage = evaluating_sentence.get_numbers_percentage()
    number_of_numbers = evaluating_sentence.get_number_of_numbers()

    #if high presence of numbers 
    if numbers_percentage > 30 and proper_nouns_percentage > 30 and (numbers_percentage + proper_nouns_percentage) > 80:
        decision_points_journal_article += 1.5
    elif number_of_numbers > 2 and proper_nouns_percentage > 30:
        decision_points_journal_article += 1
    elif number_of_numbers <= 2 and proper_nouns_percentage > 30:
        decision_points_book += 1
    elif number_of_numbers <= 1 and proper_nouns_percentage > 80:
        decision_points_book += 1.5
    else:
        decision_points_other += 1.5

    #find reference type with max points and return it
    if (decision_points_book >= decision_points_journal_article) and (decision_points_book >= decision_points_other):
        reference_category = "book"
    elif (decision_points_journal_article > decision_points_book) and (decision_points_journal_article >= decision_points_other):
        reference_category = "journal-article"
    else:
        reference_category = "other-reference"

    return reference_category