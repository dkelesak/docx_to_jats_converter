from references import Reference
from tree import Tree

#this class is supposed to be used for references that do not fall into the 3 already specified reference categories(Book, Journal Article, Online Reference)

#inherits from Reference class
class Other_Reference(Reference):
  def __init__(self, reference_id, certainty, publication_year=""):
    super().__init__(reference_id, certainty)                             
    self.publication_year = publication_year

  #create tree of this reference
  def create_tree(self):
    reference_tree = Tree("ref", "", 0, ["id", "ref-" + str(self.reference_id)])
    
    reference_tree.construct_add_child("year", self.publication_year)

    return reference_tree