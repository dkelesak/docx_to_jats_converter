from tree import Tree
import config
import docx

new_line = "\n"         

#write final tree to JATS XML file
def write_JATS_XML(document_tree):
    
    version = "1.0"       #xml version 
    encoding = "UTF-8"    #encoding type
    first_line = "<?xml version='" + version + "' encoding='" + encoding + "'?>" + new_line
    second_line = "<!DOCTYPE article PUBLIC \"-//NLM//DTD JATS (Z39.96) Journal Publishing DTD v1.1d1 20130915//EN\" \"JATS-journalpublishing1.dtd\">" + new_line

    #set filename
    output_filename = config.filename[:-5] + ".xml"
    #write file
    f = open(output_filename, "w", encoding='utf-8')
    f.write(first_line)
    f.write(second_line)
    write_tree(document_tree, f)
    f.close()


#traverse the tree and load into buffer the JATS XML file
def write_tree(node, f):                   #root calls it
    
    indentation_string = create_indentation_string(node)
   
    if node.is_root():
        temp_buffer = indentation_string + "<" + node.attribute_name +  in_tag_charasteristics_to_string(node) + ">"                                                                
        f.write(temp_buffer)
        for child in node.children:            #edo prosoxh pos kleino ta paidia
            write_tree(child, f)
        temp_buffer = new_line + indentation_string + "</" + node.attribute_name + ">"                                                                  
        f.write(temp_buffer)
    elif node.is_leaf():
        #check if leaf has content before writing it
        if(str(node.content).strip()) != "":                     
            temp_buffer = new_line + indentation_string + "<" + str(node.attribute_name) + in_tag_charasteristics_to_string(node) + ">" + str(node.content).strip() + "</" + str(node.attribute_name) + ">"      #one line tag(with content)
            f.write(temp_buffer)
        close_upper_tags(node, f)               #close upper tags if necessary
    else:
        temp_buffer = new_line + indentation_string + "<" + node.attribute_name + in_tag_charasteristics_to_string(node) + ">"                  
        f.write(temp_buffer)
        for child in node.children:           
            write_tree(child, f)
    

def in_tag_charasteristics_to_string(node):
    in_tag_charasteristics_string = ""
    
    for i in range(len(node.in_tag_characteristics)):
        #check if it contains in_tag_characteristics
        if len(node.in_tag_characteristics[i][0]) > 1:
            in_tag_charasteristics_string += " " + node.in_tag_characteristics[i][0] + "=" + "\"" + node.in_tag_characteristics[i][1] + "\"" 
        else:
            in_tag_charasteristics_string = " " + node.in_tag_characteristics[0] + "=" + "\"" + node.in_tag_characteristics[1]+ "\"" 
    
    return in_tag_charasteristics_string


#return the indentation string corresponding to this node
def create_indentation_string(node):
    indentation_level = node.depth
    indentString = ''
    indentChar ="\t"        #char to use for indent

    for n in range(indentation_level):
        indentString += indentChar
    return indentString


#if children anoigo tags -> pao sta paidia -> sto teleftaio paidi kathe fora kleino to tag toy ->kai sto telos kleino kai to arxiko tag
#closes open tags of upper levels if they need to be closed
def close_upper_tags(node, f):
    if node.is_last_child() and node.is_root() == False and node.parent.is_root() == False:
        buffer = new_line + create_indentation_string(node.parent) + "</" + node.parent.attribute_name + ">"                 #EDO THELO NANAI RECURSIVE
        f.write(buffer)
        if node.parent.is_root() == False:
            close_upper_tags(node.parent, f)
        #if node.parent.content == "":
        #    buffer = newLine + create_indentation_string(node.parent.parent) + "</" + node.parent.parent.attributeName + ">"

    #else buffer remains unchanged