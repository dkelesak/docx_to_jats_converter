from docx2python import docx2python
from docx2python.iterators import iter_paragraphs
from docx2python.text_runs import get_run_style, gather_rPr
import re
import config

####DOCX2PYTHON LIBRARY BASED FUNCTIONS####

#get document object via docx2python library without formatting info
def get_document_by_docx2python_library_without_formatting():
    document = docx2python(config.filename, html=False)
    return document

#get a list of not blank paragraphs without formatting info
def get_not_blank_paragraphs_without_formatting():
    document = get_document_by_docx2python_library_without_formatting()              
    paragraphs = list(iter_paragraphs(document.body))

    not_blank_paragraphs = []
 
    for paragraph in paragraphs:
        if paragraph.strip():
            not_blank_paragraphs.append(paragraph)

    return not_blank_paragraphs


#get document object via docx2python library with formatting info
def get_document_by_docx2python_library_with_formatting():
    document = docx2python(config.filename, html=True)
    return document

#get a list of not blank paragraphs with formatting info
def get_not_blank_paragraphs_with_formatting():
    document = get_document_by_docx2python_library_with_formatting()              
    paragraphs = list(iter_paragraphs(document.body))

    not_blank_paragraphs = []
    for paragraph in paragraphs:
        if paragraph.strip():
            not_blank_paragraphs.append(paragraph)
    
    #adjust HTML tags to JATS-XML format
    not_blank_paragraphs_fixed_html = fix_html(not_blank_paragraphs)

    return not_blank_paragraphs_fixed_html


#adjust HTML tags to JATS-XML format
def fix_html(paragraphs):
 
    paragraphs_fixed_html = []

    for paragraph in paragraphs:
        #adjust tags for bold
        paragraph = paragraph.replace("<b>","<bold>")                     
        paragraph = paragraph.replace("</b>","</bold>")
        #adjust tags for italics
        paragraph = paragraph.replace("<i>","<italic>")                  
        paragraph = paragraph.replace("</i>","</italic>")
        #adjust tags for underline
        paragraph = paragraph.replace("<u>","<underline>")                
        paragraph = paragraph.replace("</u>","</underline>")

        #remove font tag  (? -> finds shortest match)
        paragraph = re.sub(r'<font.+?>', "", paragraph)
        paragraph = re.sub(r'</font>', "", paragraph)

        paragraphs_fixed_html.append(paragraph)

    return paragraphs_fixed_html


#calculate the length of the document (max_index = document_length - 1)
def get_document_length():    #previous name->get max index
    
    document_length = len(get_not_blank_paragraphs_without_formatting())
    return document_length                                   


#returns the index of a specific element 
#input is the whole paragraph or the beginning of it
#looks for paragraphs either with formatting info as well as with no formatting nfo
def get_index_by_content(content):

    #search in list of not blank paragraphs without formatting info
    index = 0
    for paragraph in get_not_blank_paragraphs_without_formatting():
        match = re.search('^' + content +'(.+)?', paragraph)
        if match:                             #TO ADD: appropriate exceptions for images, tables if necessary
            return index
        index += 1
    
    #search in list of not blank paragraphs with formatting info
    index = 0
    for paragraph in get_not_blank_paragraphs_with_formatting():
        match = re.search('^' + content +'(.+)?', paragraph)
        if match:                             #TO ADD: appropriate exceptions for images, tables if necessary
            return index
        index += 1
    
    return None