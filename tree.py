class Tree:
    def __init__(self, attribute_name, content="", depth=0, in_tag_characteristics=[], parent=None, children=None):      #anti gia none sto content
        self.attribute_name = attribute_name
        self.content = content
        self.depth = depth
        self.in_tag_characteristics = in_tag_characteristics
        self.parent = parent
        self.children = children or []
        #index of node at its level at the tree (useful for closing tags and checking if it is the last child)
        self.index = 0
      
    #add an already created Tree as a child of the current Tree
    #Note: when merging trees call update depths function
    def add_child_object(self, child):              
        child.depth = self.depth + 1 
        #index of new child
        child.index = len(self.children) 
        self.children.append(child)
        child.parent = self
        return child     

    #create a Tree object based on the inputs and add it as a child of the current Tree
    def construct_add_child(self, attribute_name, content="", in_tag_characteristics=[]):
        depth = self.depth + 1 
        new_child = Tree(attribute_name, content, depth, in_tag_characteristics, parent=self)
        new_child.index = len(self.children) 
        self.children.append(new_child)
        return new_child
    
    #check if Tree object corresponds to a root
    def is_root(self):
        return self.parent is None

    #check if Tree object corresponds to a leaf
    def is_leaf(self):
        return not self.children

    #check if current child is last child   
    #useful in order to close appropriate tags 
    def is_last_child(self):

        if not self.is_root():
            if self.index == len(self.parent.children)-1:
                return True
            else:
                return False
        return False

    #compare if two tree objects are equal (e.g. if all their attributes are equal)
    def __eq__(self, other): 
        if not isinstance(other, Tree):
            # don't attempt to compare against unrelated types
            return print("type error")

        return (self.attribute_name == other.attribute_name) and (self.content == other.content) and (self.depth == other.depth) and (self.in_tag_characteristics == other.in_tag_characteristics) and (self.parent == other.parent) and (self.children == other.children)


#update depths, call always this functions after merging trees
def update_depths(root):
    for child in root.children:
        if root.depth >= child.depth:
            child.depth = root.depth + 1
        update_depths(child)

#print tree
#function is called by root
def print_tree(root):                
        print(root.content)
        print(root.depth)
        for child in root.children:
            print_tree(child)