import nltk, re
from reference_sentence import Reference_Sentence


#STEP 1: preprocess reference string
#preprocess reference string to prepare it for NLTK (modify it where necessary so that NLTK brings the right results)
def preprocess_reference_str(reference_str):
    #convert e.g. "pp10-20" to "pp 10 - 20 ", #necessary for nltk to handle it as number
    preprocessed_reference_str = re.sub('\d+', lambda ele: " " + ele[0] + " ", reference_str)
    return preprocessed_reference_str


#STEP 2: sentence tokenization
#nltk sentence tokenization (split in sentences)
def sentence_tokenization(preprocessed_reference_str):
    #list of sentences(in string format)
    #sentences are tokenised according to the following characters: ".", "!", "?"
    sentence_str_list  = nltk.tokenize.sent_tokenize(preprocessed_reference_str)
    #list of reference sentence objects
    sentence_objects_list = []
    for sentence_str in sentence_str_list:
        sentence_object = Reference_Sentence(sentence_str)
        sentence_objects_list.append(sentence_object)
    return sentence_objects_list


#STEP 3: sentence tokenization corrections -> check for wrong sentence tokenization and repair
def sentence_tokenization_repair(sentence_objects_list):

    #repair undersplit e.g. because of missing dots or using comma instead of dot(should have been split to more sentences)

    ###FIRST SENTENCE###  
    #-> corresponds to authors name and publication year
    #there is always at least 1 sentence
    first_sentence = sentence_objects_list[0]

    #check case if this sentence includes except author, pub. year and title as well and if so split sentence appropriately
    #if quality is ok no need to change sentence tokenisation
    #if quality is not ok then filter 
    if not quality_check_first_sentence(first_sentence):
        #filter first sentence and extract author names and year of publication
        result_dict_first_sentence = filter_first_sentence(first_sentence)
        #update string of first sentence
        updated_first_sentence = Reference_Sentence(' '.join(word for word in result_dict_first_sentence['matching-words']))
        #update sentence objects list
        sentence_objects_list.pop(0)
        sentence_objects_list.insert(0, updated_first_sentence)

        #if first sentence after filtering has a not matching part then create a new sentence 
        #at this new sentence insert the not-matching part(which was left out during filtering of first)
        if result_dict_first_sentence['not-matching-words'] != "":
            second_sentence_str = result_dict_first_sentence['not-matching-words']
            second_sentence = Reference_Sentence(second_sentence_str)
            #insert sentence (if there was already a second sentence now it becomes the third)
            sentence_objects_list.insert(1, second_sentence)
        

    ###SECOND SENTENCE###
    #->corresponds to the title
    #either already existed or it should have been created at the previous step
    second_sentence = sentence_objects_list[1]

    #check number of sentences
    number_of_sentences = len(sentence_objects_list)

    #case missing information - less information in reference than expected
    if number_of_sentences == 2:
        print("missing publication metadata")
    #case more sentences than expected: multiple title sentences (->merge) 
    elif number_of_sentences > 3:
        #merge title sentences
        updated_title_str = ""
        #iterate sentence objects list while skipping first element(corrsponding to authors,year) and last element(publication data)
        for i in range(1, number_of_sentences-1):
            #merge title reference strings
            updated_title_str += sentence_objects_list[1].sentence_str + " "
            sentence_objects_list.pop(1)
        #create new Reference_Sentence object based on the merged title string 
        updated_title_reference_sentence = Reference_Sentence(updated_title_str)
        sentence_objects_list.insert(1, updated_title_reference_sentence)

    ###THIRD SENTENCE###
    #->corresponds to publication data
    #there is no point in doing quality check for third sentence -> prepositions e.g. "Journal of" would lead to bad results.

    return sentence_objects_list


###QUALITY CHECKS of sentence objects -> True: good quality, False: needs to be filtered

#check quality of first sentence -> if bad quality filter it
def quality_check_first_sentence(reference_first_sentence):
    #check if there are words which are not proper nouns or if there are more than one numbers
    proper_nouns_percentage = reference_first_sentence.get_proper_nouns_percentage()
    number_of_numbers = reference_first_sentence.get_number_of_numbers()
    if proper_nouns_percentage == 100 and number_of_numbers == 1:
        return True
    else:
        return False

#filter first sentence and extract author names and year of publication
def filter_first_sentence(reference_first_sentence):

    #mhpws thelei deikth beginning? e.g. "^"
    # <JJ> tags is for catching names containing "-" (nltk recognises them as adjectives)
    pattern_first_sentence = "Authors_and_Year:{((<JJ><.>?<CC>?)|(<NNP><.>?<CC>?)){1,}<CD>(<.>{1,})?}"  #NNP    #use JJ for names with "-"      #sos check if CD is year                                  #{<DT>?<JJ>*<NN>}
    parser_first_sentence = nltk.RegexpParser(pattern_first_sentence)
    output_first_sentence = parser_first_sentence.parse(reference_first_sentence.tagged_words)

    #collect matching sequences
    matching_sequences = []
    for subtree in output_first_sentence.subtrees(filter=lambda t: t.label() == 'Authors_and_Year'):
        matching_words = [i[0] for i in subtree.leaves()]
        matching_sequences.append(matching_words)
    
    #number_of_matches = len(matching_sequences)

    #convert each matching sequence from list to single string
    matching_sequences_str = []
    for matching_sequence in matching_sequences:
        matching_sequence_str = ' '.join(word for word in matching_sequence)
        matching_sequences_str.append(matching_sequence_str)

    #collect not matching words into a string
    not_matching_words_str = ""
    for word in output_first_sentence:
        #if not match (matching and non matching have different lengths) and if not punctuation mark
        if len(word) == 2:
            not_matching_words_str += word[0] + " "

    #ta ypoloipa matching sequenses ti ginontai?
    result_dict_first_sentence = {'matching-words': matching_sequences_str, 'not-matching-words': not_matching_words_str}
    #sos check year 1800-2100
    return result_dict_first_sentence




#STEP 5: interpret reference and create appropriate object based on type of reference
#data has already been checked to be right
#suppose reference_sentence_objects_list has length of 3 after repair



"""
def create_online_reference(sentence_objects_list_repaired):
    #sosseonline ref kanw tetoiasub
    #  paragraph = re.sub(r'<font.+?>', "", paragraph)
    #       paragraph = re.sub(r'</font>', "", paragraph)
"""
"""
def third_sentence_quality_check():

    #books
    pattern = "NP:{<NNP>{1,}(<:>|<,>)<NNP>{1,}}"   

#get combinational textual features variables (are based on combining more than one words)
        self.other_punctuation_count = 
        self.round_brackets_count = 
"""