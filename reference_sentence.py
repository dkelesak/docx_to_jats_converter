import nltk, re

#this class is focused on analysing reference strings by employng nltk
class Reference_Sentence:
    def __init__(self, sentence_str):

        self.sentence_str = sentence_str
        #nltk word tokenize
        self.words = nltk.tokenize.word_tokenize(sentence_str)
        #nltk pos (part of speech) taggging
        #tagged_word[0] -> word string, tagged_word[1] -> part of speech(pos) string
        self.tagged_words = nltk.pos_tag(self.words)

    #update tokenize and pos_tag
    def update_word_tokenize_pos_tag(self):
        self.words = nltk.tokenize.word_tokenize(self.sentence_str)
        self.tagged_words = nltk.pos_tag(self.words)


    #get basic textual features 

    #all words
    def get_number_of_total_words(self):
        number_of_total_words = len(self.tagged_words)
        return number_of_total_words
    
    #only character words (all words except punctuation words)
    def get_number_of_character_words(self):
        number_of_character_words = 0
        for tagged_word in self.tagged_words:
            #is checking tag and not the word itself(it is more reliable this way)
            if str(tagged_word[1]).isalpha() and str(tagged_word[1]) != "CD":                      
                number_of_character_words += 1
        return number_of_character_words

    #only punctuation words
    def get_number_of_punctuation_words(self):
        number_of_punctuation_words = 0
        for tagged_word in self.tagged_words:
            #all punctuation words have non-character word tags
            if not str(tagged_word[1]).isalpha():
                number_of_punctuation_words += 1
        return number_of_punctuation_words
        
    #only nouns (NN, NNS, NNP, NNPS)
    def get_number_of_nouns(self):
        number_of_nouns = 0
        for tagged_word in self.tagged_words:
            found_noun = re.search(r'^NN', tagged_word[1])
            if found_noun:
                number_of_nouns += 1
        return number_of_nouns

    #only proper nouns (NNP, NNPS) e.g. nouns with first letter capital
    def get_number_of_proper_nouns(self):
        number_of_proper_nouns = 0
        for tagged_word in self.tagged_words:
            found_proper_noun = re.search(r'^NNP', tagged_word[1])
            if found_proper_noun:
                number_of_proper_nouns += 1
        return number_of_proper_nouns

    #only numbers
    def get_number_of_numbers(self):
        number_of_numbers = 0
        for tagged_word in self.tagged_words:
            found_number = re.search(r'^CD$', tagged_word[1])
            if found_number:
                number_of_numbers += 1
        return number_of_numbers

    #only years
    def get_number_of_years(self):
        number_of_years = 0
        for tagged_word in self.tagged_words:
            found_number = re.search(r'^CD$', tagged_word[1])
            if found_number:
                number = tagged_word[0]
                #WHAT ABOUT FLOATS?
                #found year
                if int(number) > 1800 and int(number) < 2100:
                    number_of_years += 1
        return number_of_years
    
    #only dots (e.g. ".", "!", "?")  -> no point as sentences are split according to dots

    #only commas
    def get_number_of_commas(self):
        number_of_commas = 0
        for tagged_word in self.tagged_words:
            found_comma = re.search(r'^,$', tagged_word[1])
        return number_of_commas

    #only colons
    def get_number_of_colons(self):
        number_of_colons = 0
        for tagged_word in self.tagged_words:
            found_colon = re.search(r'^:$', tagged_word[1])
        return number_of_colons

    #only parenthesis
    def get_number_of_parenthesis(self):
        number_of_parenthesis = 0
        for tagged_word in self.tagged_words:
            found_parenthesis = re.search(r'^\(|\)$', tagged_word[1])
        return number_of_parenthesis


    #get advanced textual features variables (are based on calculations between basic textual features variables)
    
    #nouns_percentage = number_of_nouns / number_of_character_words
    def get_nouns_percentage(self):
        nouns_percentage = 100 * self.get_number_of_nouns() / self.get_number_of_character_words()
        return nouns_percentage

    #proper_nouns_percentage = number_of_proper_nouns / number_of_character_words
    def get_proper_nouns_percentage(self):
        proper_nouns_percentage = 100 * self.get_number_of_proper_nouns() / self.get_number_of_character_words()
        return proper_nouns_percentage

    #numbers percentage = number of numbers / (number of numbers + number_of_character_words)
    def get_numbers_percentage(self):
        numbers_percentage = 100 * self.get_number_of_numbers() / (self.get_number_of_numbers() + self.get_number_of_character_words())
        return numbers_percentage