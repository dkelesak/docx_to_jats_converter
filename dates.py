from datetime import datetime
import re


#takes a string and if it contains a date it returns the appropriate datetime object
def get_date(date_str):

    #detect sequence of 3 numbers separated by "." or "/" or "-"
    date_match_digit_month = re.search(r'(\d+[./-]\d+[./-]\d+)', date_str)
    
    #check if date is valid and return appropriate datetime object
    #date formats considered with the following priority:
    #0: dd.mm.yyyy
    #1: mm.dd.yyyy
    #2: yyyy.mm.dd
    #3: dd/mm/yyyy
    #4: mm/dd/yyyy 
    #5: yyyy/mm/dd
    #6: dd-mm-yyyy
    #7: mm-dd-yyyy
    #8: yyyy-mm-dd   
    date_formats_digit_month = ["%d.%m.%Y", "%m.%d.%Y", "%Y.%m.%d", "%d/%m/%Y", "%m/%d/%Y", "%Y/%m/%d", "%d-%m-%Y", "%m-%d-%Y", "%Y-%m-%d"]        
 
    for date_format in date_formats_digit_month:
        try:
            #TO DO: remove spaces from date_match.group()
            date = datetime.strptime(date_match_digit_month.group(), date_format)
            return date
        except:
            continue

    
    #detect sequence of one number followed by a month's name(full or abbreviated form) and another number
    date_match_nondigit_month = re.search(r'\d+ (?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?)\.?\,? ?\d+', date_str)

    #check if date is valid and return appropriate datetime object
    #date formats (with name in character format) considered with the following priority:
    #example date: 30.3.2019
    #0: 30 March 2019
    #1: 30 March,2019
    #2: 30 March, 2019
    #3: 30 Mar 2019
    #4: 30 Mar. 2019
    #5: 30 Mar,2019 
    #6: 30 Mar, 2019
    #7: 30 Mar.,2019 
    #8: 30 Mar., 2019

    date_formats_nondigit_month = ["%d %B %Y", "%d %B,%Y", "%d %B, %Y", "%d %b %Y", "%d %b. %Y", "%d %b,%Y", "%d %b, %Y",  "%d %b.,%Y", "%d %b., %Y"] 

    for date_format in date_formats_nondigit_month:
        try:
            #TO DO: remove spaces from date_match.group()
            date = datetime.strptime(date_match_nondigit_month.group(), date_format)
            return date
        except:
            continue

    #if no format matched
    return None 


#converts datetime object to date string in iso-8601 format (yyyy-mm-dd)
def convert_date_to_iso8601_str(date):
    date_str_iso8601 = date.isoformat()
    #keep only the date part not the time part (e.g. until finding the character "T")
    date_str_iso8601 = date_str_iso8601.split("T")[0]
    return date_str_iso8601


"""
#takes a string and returns the first year in parenthesis that it detects, in string format
def get_year_in_parenthesis(year_str):
    year_in_parenthesis_match = re.compile(r'\(([1-2][0-9]{3}|3000)\)')
    if year_in_parenthesis_match:
        match = year_in_parenthesis_match.search(reference_str)                       
        position = match.start()
        year = match.group()

#takes a string and returns the first year that it detects, in string format
def get_year(year_str):
    #find first year in parenthesis ->usually is the publication date 

#else

#find first year appearing in string, with or without parenthesis
year_match = re.compile(r'[1-2][0-9]{3}|3000')
if year_match:
    match = year_match.search(reference_str)                       
    position = match.start()
    year = match.group()
#else
"""