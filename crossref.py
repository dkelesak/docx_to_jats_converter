import requests, json
from book import Book
from journal_article import Journal_Article
from online_reference import Online_Reference


#reference_str -> initial reference string taken from the docx
def get_crossref_reference_object(reference_id, reference_str):

    crossref_response_json = get_crossref_json_data(reference_str)
    
    #check if status is ok
    if crossref_response_json['status'] == 'ok':
        
        #get type of reference
        type_of_reference = categorise_crossref_reference(crossref_response_json)
        
        #call appropriate function depending on type of reference
        if type_of_reference == "journal-article":
            journal_article = create_crossref_journal_article(reference_id, crossref_response_json)
            return journal_article

        elif type_of_reference == "book":
            book = create_crossref_book(reference_id, crossref_response_json)
            return book
        #else:

        #did not get crossref data successfully
        else:
            print("Error retrieving data - crossref probably denied connection. Maybe your email should be added to the https request")

    return None
    

#send https request and get crossref metadata in JSON format
def get_crossref_json_data(reference_str):
       
    #convert special characters to their corresponding URL code
    reference_str_replace = convert_reference_str_to_url(reference_str)

    #send request - (rows=1 to return only first result)
    crossref_response = requests.get('https://api.crossref.org/works?rows=1&query='+reference_str_replace)
    print('https://api.crossref.org/works?rows=1&query='+reference_str_replace)

    #parse JSON
    crossref_response_text = crossref_response.text                     #JSON raw
    crossref_response_json = json.loads(crossref_response_text)         #JSON object

    return crossref_response_json


#convert special characters to their corresponding URL code
def convert_reference_str_to_url(reference_str):

    url_coding = {';': '%3B', '/': '%/2F', 
                '?': '%/3F', ':': '%3A',
                '@': '%40', '=': '%3D',
                '&': '%26'}

    for k, v in url_coding.items():
        reference_str_replace = reference_str.replace(k, v)

    return reference_str_replace


###DATA RELATED FUNCTONS###
#get type of reference (book, journal article etc.)
def categorise_crossref_reference(crossref_response_json):
    type_of_reference = crossref_response_json["message"]["items"][0]["type"]
    return type_of_reference


#
def create_crossref_book(reference_id, crossref_response_json):
    
    title = crossref_response_json["message"]["items"][0]["title"][0]
    subtitle = crossref_response_json["message"]["items"][0]["subtitle"][0]
    source = title + " " + subtitle

    authors_list = [] 
    authors = crossref_response_json["message"]["items"][0]["editor"]         
    for author in authors:
        surname = author['family']
        given_names = author['given']
        author_dictionary = {"prefix": "", "surname": surname, "given-names": given_names, "suffix": ""}    
        authors_list.append(author_dictionary)

    publication_year = crossref_response_json["message"]["items"][0]["issued"]["date-parts"][0][0]
    publisher_name = crossref_response_json["message"]["items"][0]["publisher"]

    #FIX PUBLISHER LOCATION - MAYBE EMPTY SOMETIMES
    publisher_location = ""

    book = Book(reference_id, authors_list, publication_year, source, publisher_name, publisher_location)
    
    return book


#
def create_crossref_journal_article(reference_id, crossref_response_json):

    title = crossref_response_json["message"]["items"][0]["title"][0]
    
    authors_list = [] 
    authors = crossref_response_json["message"]["items"][0]["author"]        
    for author in authors:
        surname = author['family']
        given_names = author['given']
        author_dictionary = {"prefix": "", "surname": surname, "given-names": given_names, "suffix": ""}    #<- LIST OF DICTS
        authors_list.append(author_dictionary)
    
    publication_year = crossref_response_json["message"]["items"][0]["published-print"]["date-parts"][0][0]
    source = crossref_response_json["message"]["items"][0]["short-container-title"][0]
    volume = crossref_response_json["message"]["items"][0]["volume"]
    #issue???
    
    pages = crossref_response_json["message"]["items"][0]["page"]                   #multiple pages?
    #fpage and lpage are separated by "-"
    split_pages = pages.split("-")
    fpage = split_pages[0]
    if len(split_pages) >  1:
        lpage = split_pages[1]
    else:
        lpage =""

    #FIX ISSUE
    issue = 0

    journal_article = Journal_Article(reference_id, title, authors_list, publication_year, source, volume, issue, fpage, lpage)
   
    return journal_article


#compare crossref and mined object
#def validate_metadata_via_crossref():