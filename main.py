import os.path, re
import config
from read_docx import create_document_tree
from write_JATSXML import write_JATS_XML

check_file_type = re.search(r'\.docx$', config.filename)
check_file_existence = os.path.isfile(config.filename)

#wrong type of file
if not check_file_type:
    print("You inserted a wrong type of file for conversion.") 
    print("Solution: Please specify a .docx file at config.py script.")

#file does not exist
elif not check_file_existence: 
    print("You inserted at config.py the name of a file that does not exist.") 
    print("Solution: Please specify a file existing at the current folder.")

#execute the programme normally
else:
    #read file and get document tree
    document_tree = create_document_tree()
    #write jats xml file based on document tree
    write_JATS_XML(document_tree)
    #successful conversion
    print("File has been converted successfully to JATS-XML.") 
    print("Output file has been saved at the current folder with name: " + config.filename[:-5] + ".xml")