import re
from document_docx2python_library import get_not_blank_paragraphs_with_formatting
from tree import Tree

class List:
    def __init__(self, list_index, list_style, list_items, paragraph_index):      
        self.list_index = list_index
        self.list_id = list_index + 1         #list ids start from 1 instead of 0
        self.list_style = list_style
        self.list_items = list_items
        self.paragraph_index = paragraph_index


#scans the document and returns a list of list objects 
def get_list_object(index, list_index):

    #document = get_document_by_docx2python_library()
    paragraphs = get_not_blank_paragraphs_with_formatting()

    list_style = "bullet"
    list_items = []
    temp_index = index    #index ->corresponds to paragraph index

    while re.search(r'^--\t', str(paragraphs[temp_index])):            #while list item
        #remove list marker used by doc2python
        paragraph_strip = re.sub(r'^--\t', '', str(paragraphs[temp_index]))                 
        list_items.append(paragraph_strip)
        temp_index += 1
    #end of list

    #create list object
    list_object = List(list_index, list_style, list_items, index)     #index ->corresponds to paragraph index

    return list_object


#transforms list object into a tree-format
def create_list_tree(list_object):
    
    list_tree = Tree( "list", "", 0, [["id", "list-"+str(list_object.list_id)], ["list-type", list_object.list_style]])

    for list_item in list_object.list_items:
        list_node = Tree("list-item")
        list_node.construct_add_child("p", list_item)
        list_tree.add_child_object(list_node)

    return list_tree