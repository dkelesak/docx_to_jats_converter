import re, zipfile
import config
from document_docx2python_library import get_not_blank_paragraphs_with_formatting
from tree import Tree

class Figure:
    def __init__(self, figure_index, caption, figure_filename, paragraph_index):      
        self.figure_index = figure_index
        self.figure_id = figure_index + 1         #captions of figures inside the docx file start from 1 instead of 0
        self.caption = caption
        self.figure_filename = figure_filename
        self.paragraph_index = paragraph_index
       

#extract and save docx images at a folder named "images"
def extract_save_figures():

    archive = zipfile.ZipFile(config.filename)
    for file in archive.filelist:
        if file.filename.startswith('word/media/'): 
            #create an /images file and save extracted images there
            file.filename = re.sub('^word/media/', "images/", file.filename)                         
            archive.extract(file)
        

#scans the document and returns list of figure objects 
def get_figure_objects_list():

    #document = get_document_by_docx2python_library()
    paragraphs = get_not_blank_paragraphs_with_formatting()

    figure_objects_list = []
    figure_index = 0
    paragraph_index = 0
    
    for paragraph in paragraphs:
        paragraph_str = str(paragraph)

        ###maybe this part not necessary###
        #remove temporarily opening tags at beginning of string
        found_initial_tags = re.search(r'(<.+?>){1,}', paragraph)
        if found_initial_tags:
            initial_tags_str = found_initial_tags.group()
            paragraph = paragraph[found_initial_tags.end():]
        #remove temporarily closing tags at end of string
        found_final_tags = re.search(r'(</.+?>){1,}', paragraph)
        if found_final_tags:
            final_tags_str = found_final_tags.group()
            paragraph = paragraph[found_final_tags.end():]


        #figure format: "----media/image1.jpeg----""
        figure_starting_marker = "----media/"
        figure_ending_marker = "----"

        #if figure
        if paragraph.strip().startswith(figure_starting_marker) and paragraph.strip().endswith(figure_ending_marker):
            figure_filename = paragraph_str[len(figure_starting_marker):len(paragraph_str)-len(figure_ending_marker)]
            caption = paragraphs[paragraph_index-1] #caption is in previous paragraph e.g. above the figure
            figure = Figure(figure_index, caption, figure_filename, paragraph_index)
            figure_objects_list.append(figure)

            figure_index += 1

        paragraph_index += 1

    print(len(figure_objects_list))
    return figure_objects_list 
    

#transforms table into a tree-format
def create_figure_tree(figure):

    figure_tree = Tree("fig", "", 0, ["id", "fig-" + str(figure.figure_id)])

    figure_tree.construct_add_child("label", "Figure " + str(figure.figure_id))
    caption_node = figure_tree.construct_add_child("caption", figure.caption)
    figure_tree.construct_add_child("p", figure.figure_filename)
    #caption_node.construct_add_child("title", paragraph)                   #table caption
    
    #fig_node.construct_add_child("graphic", "", ALLA ATTRIBUTES)

    return figure_tree