#2nd and 3rd sentence are usually split right
#quality check 2nd sentence -> check if has 3 different tags or more, number of nnps

#filter first sentence and extract author names and year of publication
def filter_third_sentence(reference_third_sentence):

    reference_third_sentence = Reference_Sentence(reference_third_sentence)
    #mhpws thelei deikth beginning? e.g. "^"

    #step 1:check number of numbers & check crossref & check for words like ed journal book
    #cross ref type check
    #local type check

    #to thema einai oti ena komma sto xala   loose nltk
    #step 2:journal article
    pattern_third_sentence = "Journal_Article_Numbers:{<CD>(<\(><CD><\)>)?<.><CD><:><CD>}"  #NNP  #use JJ for names with "-"  #sos check if CD is year                                  #{<DT>?<JJ>*<NN>}
    parser_third_sentence = nltk.RegexpParser(pattern_third_sentence)
    output_third_sentence = parser_third_sentence.parse(reference_third_sentence.tagged_words)

    #collect matching sequences
    matching_sequences = []
    for subtree in output_third_sentence.subtrees(filter=lambda t: t.label() == 'Journal_Article_Numbers'):
        matching_words = [i[0] for i in subtree.leaves()]
        matching_sequences.append(matching_words)

    """
    #step 2: book case 
    #SOS AFTO THELEI SPASIMO SE 2 CHUNK AN O ALLOS VALEI London, United Kingdom tha katarefsei
    pattern_third_sentence = "Book_Publisher_Location:{(<\(>?(<JJ>|<NNP>|<NNPS>)<\)>?){1,}}"  #NNP  #use JJ for names with "-"  #sos check if CD is year                                  #{<DT>?<JJ>*<NN>}
    parser_third_sentence = nltk.RegexpParser(pattern_third_sentence)
    output_third_sentence = parser_third_sentence.parse(reference_third_sentence.tagged_words)
    
    #collect matching sequences
    matching_sequences = []
    for subtree in output_third_sentence.subtrees(filter=lambda t: t.label() == 'Book_Publisher_Location'):
        matching_words = [i[0] for i in subtree.leaves()]
        matching_sequences.append(matching_words)
"""

# ----SOS--- entometaksy remove stopwords ##
 #SOS REMOVE STOPWORDS DONT ADD THEM AS NOT MATCHING TO SENTENCE 2
#mporo na exw multiple patterns
#should have previously done word tagging to use NLTK RegexpParser
#simplify regex

#handle tuples
#patterns = "P:{<NNP><CD>}"        #(<CC><JJ>)?}"
#cp = nltk.RegexpParser('CHUNK:{<NNP><NNP><NNP>}')                  # #{<V.*> <TO> <V.*>}')



#online ref hyperlink fix
 #paragraph = re.sub(r'<font size=\"\d+\">', '', paragraph)          #adjust tags for font
    #paragraph = paragraph.replace("</font>","")                  #.strip()

    #remove unnecessary tags
    #remove hyperlink tag
    #paragraph = "<a href=\"https://github.com/PeerJ/jats-conversion/blob/master/tests/example-jats-1-0.xml\">"
    #paragraph = re.sub(r'<a href=\".*\"\>>', '', paragraph)
    #paragraph = paragraph.replace("</a>", "")
    #sos GRAMMATOSEIRA SIZE TO KANW /2