from back import create_back_tree, find_references
from body import create_body_tree, find_body_beginning
from front import create_front_tree, get_abstract_index
from tree import Tree, update_depths


#scan all the text and recognise elements
#returns the final tree in order to be written to file
def create_document_tree():

    article_tree = Tree("article")
    front_body_back_bounds_dict = split_front_body_back()     #get dictionary of index bounds of front, body, back
    
    #get sub-trees for front, body, back (if they exist) and add them to article_tree
    #if there is front
    if front_body_back_bounds_dict['front']['beginning'] is not None and front_body_back_bounds_dict['front']['end'] is not None:                    
        front_tree = create_front_tree(front_body_back_bounds_dict['front']['beginning'], front_body_back_bounds_dict['front']['end'])
        #add sub-tree to article tree
        article_tree.add_child_object(front_tree)
    #if there is body
    if front_body_back_bounds_dict['body']['beginning'] is not None and front_body_back_bounds_dict['body']['end'] is not None:
        body_tree = create_body_tree(front_body_back_bounds_dict['body']['beginning'], front_body_back_bounds_dict['body']['end'])
        #add sub-tree to article tree
        article_tree.add_child_object(body_tree)
    
    """
    #if there is back
    if front_body_back_bounds_dict['back']['beginning'] is not None and front_body_back_bounds_dict['back']['end'] is not None:
        back_tree = create_back_tree(front_body_back_bounds_dict['back']['beginning'], front_body_back_bounds_dict['back']['end'])
        
        #HERE ADD STYLING INFO TO BACK TREE WITH THE HELP OF THE OTHER SCRIPT
        
        #add sub-tree to article tree
        article_tree.add_child_object(back_tree)
    """

    #some depths during merging trees are not updated it automatically - refresh them
    update_depths(article_tree)

    return article_tree


#find the bounds of front, body and back 
def split_front_body_back():
    
    #front
    #if there is no abstract skip front
    front_beginning = None
    front_end = None
    #if found abstract set front beginning and end bounds
    if get_abstract_index() is not None:
        front_beginning = 0

        abstract_beginning = get_abstract_index()
        abstract_end = find_body_beginning(abstract_beginning) - 1          
        front_end = abstract_end

    #body
    body_beginning = 0
    #if found body beginning - section title
    if find_body_beginning(abstract_beginning) is not None:
        body_beginning = find_body_beginning(abstract_beginning)        
    body_end = None
    #if found references
    if find_references() is not None:
        body_end = find_references() - 1
    else:
        body_end = get_document_length() - 1     
    
    #back
    back_beginning = find_references()
    back_end = None
    #if found references
    if back_beginning is not None:
        back_end = get_document_length() - 1  #max index = length - 1

    #dictionary of index bounds for front, body, back
    front_body_back_bounds_dict = {'front' : {'beginning' : front_beginning, 'end' : front_end},
                'body' : {'beginning' : body_beginning, 'end' : body_end},
                'back' : {'beginning' : back_beginning, 'end' : back_end}}
    
    print(front_body_back_bounds_dict)

    return front_body_back_bounds_dict