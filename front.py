from authors import get_authors_dictionaries
from document_docx2python_library import get_not_blank_paragraphs_with_formatting, get_not_blank_paragraphs_without_formatting, get_index_by_content
from tree import Tree

#create a tree corresponding to the front
def create_front_tree(front_beginning, front_end):
    front_node = Tree("front")
    #journal meta
    journal_meta_tree = create_journal_meta_tree()
    front_node.add_child_object(journal_meta_tree)
    #article meta
    article_meta_tree = create_article_meta_tree(front_end)
    front_node.add_child_object(article_meta_tree)
    #authors
    authors_tree = create_authors_tree()  
    article_meta_tree.add_child_object(authors_tree)

    return front_node


#create a tree corresponding to journal metadata
def create_journal_meta_tree():

    #enter here journal metadata
    journal_id = "peerj-cs"
    journal_title = "PeerJ Computer Science"
    abbrev_journal_title = "PeerJ Comput. Sci."
    issn = "2376-5992"
    publisher_name = "PeerJ Inc."
    publisher_loc = "San Francisco, USA"

    #create journal-meta tree
    journal_meta_tree = Tree("journal-meta")
    journal_meta_tree.construct_add_child("journal-id", journal_id, ["journal-id-type", "publisher-id"])
    journal_title_group_node = journal_meta_tree.construct_add_child("journal-title-group")
    journal_title_group_node.construct_add_child("journal-title", journal_title)
    journal_title_group_node.construct_add_child("abbrev-journal-title", abbrev_journal_title, ["abbrev-type", "publisher"])
    journal_meta_tree.construct_add_child("issn", issn, ["pub-type", "epub"])
    publisher_node = journal_meta_tree.construct_add_child("publisher")
    publisher_node.construct_add_child("publisher-name", publisher_name)
    publisher_node.construct_add_child("publisher-loc", publisher_loc)

    return journal_meta_tree


#create a tree corresponding to article metadata
def create_article_meta_tree(front_end):

    #enter here article metadata
    article_id_type_publisher_id = "cs-52"
    article_id_type_doi = "10.7717/peerj-cs.52"
    article_categories_subject_list = ["Data Science", "Databases"]
    pub_date = "2016-03-30"       #iso 8601 format
    volume = "2"
    elocation_id = "e52"
    #permissions
    copyright_statement = "2016 MacDonald"
    copyright_year = "2016"
    copyright_holder = "MacDonald"
    lisence_p = "This is an open access article distributed under the terms of the <ext-link ext-link-type=\"uri\" xlink:href=\"http://creativecommons.org/licenses/by/4.0/\">Creative Commons Attribution License</ext-link>, which permits unrestricted use, distribution, reproduction and adaptation in any medium and for any purpose provided that it is properly attributed. For attribution, the original author(s), title, publication source (PeerJ Computer Science) and either DOI or URL of the article must be cited."
    #funding
    funding_statement = "The author received no funding for this work."
    #end of enter here article metadata


    #rest of the article metadata is extracted by the programme (e.g. title, authors)

    #create article-meta tree
    article_meta_tree = Tree("article-meta")
    article_meta_tree.construct_add_child("article-id", article_id_type_publisher_id, ["pub-id-type", "publisher-id"])
    article_meta_tree.construct_add_child("article-id", article_id_type_doi, ["pub-id-type", "doi"])
    article_categories_node = article_meta_tree.construct_add_child("article-categories")
    subject_group_node = article_categories_node.construct_add_child("subj-group", "", ["subj-group-type", "categories"])
    for subject in article_categories_subject_list:
        subject_group_node.construct_add_child("subject", subject)

    #article title and authors are automatically extracted by the programme
    title_group_node = article_meta_tree.construct_add_child("title-group")
    title_node = get_title_node()
    title_group_node.add_child_object(title_node)
    contrib_group_authors_tree = create_authors_tree()
    article_meta_tree.add_child_object(contrib_group_authors_tree)

   
    #continuing creating article-meta tree
    publication_date_node = article_meta_tree.construct_add_child("pub-date", "", [["pub-type", "epub"], ["date-type", "pub"], ["iso-8601-date", pub_date]])
    #split pub_date string (iso 8601 format, yyyy-mm-dd)
    pub_date_split = pub_date.split("-")
    publication_date_node.construct_add_child("day", pub_date_split[2])
    publication_date_node.construct_add_child("month", pub_date_split[1])
    publication_date_node.construct_add_child([["year", pub_date_split[0]], ["iso-8601-date", pub_date_split[0]]])

    article_meta_tree.construct_add_child("volume", volume)
    article_meta_tree.construct_add_child("elocation-id", elocation_id)
    history_node = article_meta_tree.construct_add_child("history")
    #permissions sub tree
    permissions_node = article_meta_tree.construct_add_child("permissions")
    permissions_node.construct_add_child("copyright-statement", copyright_statement)
    permissions_node.construct_add_child("copyright-year", copyright_year)
    permissions_node.construct_add_child("copyright-holder", copyright_holder)
    #lisence sub tree
    lisence_node = permissions_node.construct_add_child("lisence", "", [["license-type", "open-access"], ["xlink:href", "http://creativecommons.org/licenses/by/4.0/"]])
    lisence_node.construct_add_child("lisence-p", lisence_p)

    
    abstract_index = get_abstract_index()            #find index of abstract title
    abstract_node = create_abstract_tree(abstract_index, front_end)
    article_meta_tree.add_child_object(abstract_node)                                 
  

    kwd_group_node = article_meta_tree.construct_add_child("kwd-group", "", ["kwd-group-type", "author"])
    keyword_list = ["keyword1string", "keyword2string", "keyword3string", "keyword4string"]
    for keyword in keyword_list:
        kwd_group_node.construct_add_child("kwd", keyword)

    funding_group_node = article_meta_tree.construct_add_child("funding-group")
    funding_group_node.construct_add_child("funding-statement", "The author received no funding for this work.")

    return article_meta_tree


#get authors subtree  
def create_authors_tree():
    
    authors_str = get_not_blank_paragraphs_without_formatting()[1]
    #list of authors' dictionaries
    authors_dictionary_list = get_authors_dictionaries(authors_str)

    #create root
    authors_tree = Tree("contrib-group", "", 0, ["content-type", "authors"])

    author_index = 0     

    for author in authors_dictionary_list:

        author_tree = authors_tree.construct_add_child("contrib", "", [["id", "author-"+str(author_index)], ["contrib-type", "author"], ["corresp", "yes"]])

        name_tree = author_tree.construct_add_child("name")
        #surname and given-names appear in the opposite order than at references
        #improve following piece of code style
        name_tree.construct_add_child("surname", author["given-names"])
        name_tree.construct_add_child("given-names", author["surname"])
        if author["prefix"] != "":
            name_tree.construct_add_child("prefix", author["prefix"])
        if author["suffix"] != "":
            name_tree.construct_add_child("suffix", author["suffix"])

        author_tree.construct_add_child("email", "mikeauthor@gmail.com")
        author_index += 1

    return authors_tree


#get the node of the article title 
def get_title_node():
    #suppose title is at the first paragraph
    title = get_not_blank_paragraphs_with_formatting()[0]
    #create appropriate node
    title_node = Tree("article-title", title)
    return title_node
    

#returns the index of the beginning of the abstract
def get_abstract_index():
    abstract_names = ["Abstract", "Abstrakt", "Zusammenfassung", "Auszug"]
    for name in abstract_names:
        index = get_index_by_content(name)
        if index != None:
            return index
    print("No abstract found!")
    return None


#get the tree of the abstract
def create_abstract_tree(abstract_index, front_end):
    #create appropriate nodes
    abstract_node = Tree("abstract")
    for temp_index in range (abstract_index+1, front_end+1):                       #include front_end as well
        abstract_paragraph = get_not_blank_paragraphs_with_formatting()[temp_index]
        abstract_node.construct_add_child("p", abstract_paragraph)
    return abstract_node