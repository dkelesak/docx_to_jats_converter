import re
from authors import get_authors_dictionaries, create_authors_tree
from reference import Reference
from tree import Tree

#inherits from Reference class
class Book(Reference):
  def __init__(self, reference_id, certainty, authors_dictionary_list=[], publication_year="", source="", publisher_name="", publisher_location=""):
    super().__init__(reference_id, certainty)
    self.authors_dictionary_list = authors_dictionary_list                                   
    self.publication_year = publication_year
    self.source = source
    self.publisher_name = publisher_name
    self.publisher_location = publisher_location

  #create tree of this reference
  def create_tree(self):
    reference_tree = Tree("ref", "", 0, ["id", "ref-" + str(self.reference_id)])
    
    #create tree including the names of all the authors
    authors_tree = create_authors_tree(self.authors_dictionary_list)
    reference_tree.add_child_object(authors_tree)

    reference_tree.construct_add_child("year", self.publication_year)
    reference_tree.construct_add_child("source", self.source)
    reference_tree.construct_add_child("volume", self.publisher_name)
    reference_tree.construct_add_child("issue", self.publisher_location)

    return reference_tree

  
#takes a reference in the form of a list of 3 Reference_Sentence objects, analyses it and returns the corresponding Book object
def create_book(sentence_objects_list_repaired, reference_id):
    
    #first sentence
    authors_year_str = sentence_objects_list_repaired[0].sentence_str
    #split to authors and year
    year_match = re.compile(r'(\()?([1-2][0-9]{3}|3000)(\))?')
    if year_match:
        match = year_match.search(authors_year_str)                       
        year_position = match.start()
        publication_year = match.group()
    authors_str = authors_year_str[:year_position]
    authors_dictionary_list = get_authors_dictionaries(authors_str)

    #second sentence
    source = sentence_objects_list_repaired[1].sentence_str
    
    #third sentence
    publisher_details_str = sentence_objects_list_repaired[2].sentence_str
    publisher_dictionary = interpret_book_publisher_details(publisher_details_str)
    publisher_name = publisher_dictionary["publisher-name"]
    publisher_loc = publisher_dictionary["publisher-loc"]

    book_reference = Book(reference_id, authors_dictionary_list, publication_year, source, publisher_name, publisher_loc)

    return book_reference


#interpret book publisher details string(corresponding to Sentence 3 of sentence_objects_list)
#returns publisher_dictionary(publisher_name, publisher_location)  
def interpret_book_publisher_details(publisher_details_str):
   
    #publisher name and publisher location can be dot, double dot or comma separated
    publisher_name = ""
    publisher_location = ""

    #take 2 last sentences
    reference_split = publisher_details_str.split(".")
    #if empty sentence
    if reference_split[-1].strip() == "":
        reference_split.pop(-1)

    #check for digits
    digit_check = re.search(r'\d+', reference_split[-1])

    if not digit_check:                                             
        publisher_split_by_double_dot = reference_split[-1].split(":")
        publisher_split_by_comma = reference_split[-1].split(",")

        #case: name and location splt by ":"
        if len(publisher_split_by_double_dot) > 1:
            publisher_name = publisher_split_by_double_dot[0].strip()                                     
            publisher_location = publisher_split_by_double_dot[1].strip()
        #case: name and location splt by ","
        elif len(publisher_split_by_comma) > 1:
            publisher_name = publisher_split_by_comma[0].strip()
            publisher_location = publisher_split_by_comma[1].strip()
        #case: name and location splt by "."
        elif len(reference_split) > 1:                                                              
            publisher_name = reference_split[-2].strip() 
            publisher_location = reference_split[-1].strip()
        #case: only publisher is available
        else:                                                                
            publisher_name = reference_split[-1].strip() 
            publisher_location = ""

    publisher_dictionary = {"publisher-name": publisher_name, "publisher-loc": publisher_location}

    return publisher_dictionary  