import re
import nltk
from collections import Counter 

#remove stop words etc. - not important words
#1 diafonia epitrepetai

#things that can be fixed:
#I supposed tags without attributes inside tag to keep it simple
#I supposed styling exists only at word level

reference_str_html = "In <bold>welchen mata</bold> Rama Rechtsverfahren<italic>sie <bold>Parteistellung</bold></italic> erlangen und welche Rechte<underline><bold> ihnen</bold></underline> zukommen."
reference_str = "In welchen mata Rechtsverfahren sie Parteistellung erlangen und welche Rechte ihnen zukommen."

#split words -> vash node.content
#words_list = node.content.split(" ")
words_list = ["welchen", "mata", "Rama", "Rechtsverfahren", "Parteistellung"]
words_list1 = ["Rechte", "ihnen", "zukommen"]
words_list_including_tags = []


#iterate words
for word in words_list:

    #find desired word position at html string
    found_word = re.search(word, reference_str_html)
    if found_word:
        word_position = re.search(word, reference_str_html).start()

        #extract tags until that position
        extract_opening_tags = re.findall(r'<[^</]*?>', reference_str_html[:word_position])
        extract_closing_tags = re.findall(r'</.*?>', reference_str_html[:word_position])
        number_of_opening_tags = len(extract_opening_tags)
        number_of_closing_tags = len(extract_closing_tags)

        #create list of tag names alone - keep only names of html tags
        processed_opening_tags = []
        processed_closing_tags = []

        #process appropriately opening tags
        for opening_tag in extract_opening_tags:
            #fix this   #sos to apo kato na mpei se if
            processed_opening_tags.append(re.search(r'<.*?( |>)', opening_tag).group()[1:-1])
        #process appropriately closing tags
        for closing_tag in extract_closing_tags:
            closing_tag = closing_tag.strip(">")
            processed_closing_tags.append(re.sub(r'</', r'', closing_tag))
      
        #find html tags which are opened but not closed
        not_closed_opening_tag_names_list = []
        
        if number_of_opening_tags > number_of_closing_tags:
            #get tags which are opened but not closed (e.g. which are in opening tags list but not in closing tags list)
            not_closed_opening_tag_names_list = list((Counter(processed_opening_tags) - Counter(processed_closing_tags)).elements())

        #create a list of tags to be placed to this word at the right format and order
        not_closed_opening_tags = []
        not_closed_closing_tags = []

        for not_closed_opening_tag in not_closed_opening_tag_names_list:
            #place < and > at beginning and end of processed tag names accordingly
            not_closed_opening_tags.append("<"+not_closed_opening_tag+">")

        #for closing tags iterate on the opposite direction
        for not_closed_opening_tag in reversed(not_closed_opening_tag_names_list):   
            #place </ and > at beginning and end of processed tag names accordingly
            not_closed_closing_tags.append("</"+not_closed_opening_tag+">")

        #place tags to word
        word_with_tags = ''.join(not_closed_opening_tags) + word + ''.join(not_closed_closing_tags)
        #create list of words with their tags
        words_list_including_tags.append(word_with_tags)

        #remove desired_word from initial html string 
        #this helps avoid errors in case same word appears twice at the string
        reference_str_html = re.sub(word, "", reference_str_html, 1)

#check an EINAI KALO TO TAB BACK
#turn list to string and introduce spaces between different words
desired_string_with_tags = ' '.join(words_list_including_tags)

#remove close open same tags, delete consecutive same tags merge tags
consecutive_tags = re.compile(r'</.*?> <.*?>')
for match in consecutive_tags.finditer(desired_string_with_tags):
    #extract tag names
    consecutive_tags = match.group().split("> <")
    
    #extract closing tag name
    consecutive_tags[0] = str(consecutive_tags[0]).strip("</")
    #extract opening tag name
    consecutive_tags[1] = str(consecutive_tags[1]).strip(">")

    #check if extracted tag names are the same
    if consecutive_tags[0] ==  consecutive_tags[1]:
        #if they are the same remove both of them - they shoudn't be there
        desired_string_with_tags_fixed = desired_string_with_tags[:match.start()] + " " + desired_string_with_tags[match.end():] 

#remove spaces surrounding tags??


"""
#iterate tags which are not closed
for not_closed_opening_tag in not_closed_opening_tags_list:
    #iterate initial list of opening tags (which are either closed or not)
    for opening_tag in extract_opening_tags:
        found_extracted_tag = re.search(not_closed_opening_tag, opening_tag)
        if found_extracted_tag:
            break
"""