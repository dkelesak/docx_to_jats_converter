from authors import get_authors_dictionaries
from book import create_book
from categorise_reference import categorise_reference
from document_docx2python_library import get_not_blank_paragraphs_without_formatting, get_index_by_content
from journal_article import create_journal_article
from online_reference import create_online_reference
from sentence_tokenisation import preprocess_reference_str, sentence_tokenization, sentence_tokenization_repair
from tree import Tree

#returns the index of the beginning of references
def find_references():     
    # and index should be > body index            
    references_names = ["References", "Quellen", "Bibliographie", "Bibliografie"]
    for name in references_names:
        temp_index = get_index_by_content(name)
        if temp_index != None:
            return temp_index
    return None


#create tree corresponding to the back 
def create_back_tree(back_beginning, back_end):
  
    reference_id = 0
    #create root
    back_tree = Tree("back")

    ack_node = back_tree.construct_add_child("ack")
    ack_node.construct_add_child("p", "I would like to thank Di MacDonald for her editorial advice on various drafts.")
    additional_information_node = back_tree.construct_add_child("sec", "", ["sec-type", "additional-information"])
    additional_information_node.construct_add_child("title", "Additional Information and Declarations")
    competing_interests_node = additional_information_node.construct_add_child("fn-group", "", ["content-type", "competing-interests"])
    competing_interests_node.construct_add_child("title", "Competing Interests")

    ref_list_tree = back_tree.construct_add_child("ref-list", "", ["content-type", "authoryear"])
    ref_list_tree.construct_add_child("title", "References")

    #avoid first paragraph of back which corresponds to "References" title
    #include last paragraph of back which corresponds to the last reference
    for paragraph in get_not_blank_paragraphs_without_formatting()[back_beginning+1:back_end+1]:              
        reference_id += 1
        reference_str = paragraph
        #get reference object
        reference_object = get_reference_object(reference_str, reference_id)
        #get reference tree of this reference
        reference_tree = reference_object.create_tree()
        ref_list_tree.add_child_object(reference_tree)

        #TO DO: ADD REFerence formating from respective class

    return back_tree


def get_reference_object(reference_str, reference_id):

    #
    preprocessed_reference_str = preprocess_reference_str(reference_str)
    #
    sentence_objects_list = sentence_tokenization(preprocessed_reference_str)
    #
    sentence_objects_list_repaired = sentence_tokenization_repair(sentence_objects_list)
    #get reference category and create appopriate function to create reference object
    reference_category = categorise_reference(preprocessed_reference_str, sentence_objects_list_repaired)
    if reference_category == "book":
        reference_object = create_book(sentence_objects_list_repaired, reference_id)
    elif reference_category == "journal-article":
        reference_object = create_journal_article(sentence_objects_list_repaired, reference_id)
    elif reference_category == "online-reference":
        reference_object = create_online_reference(sentence_objects_list_repaired, reference_id)
    #elif reference_category == "other-reference":
    #    reference_object = create_other_reference(sentence_objects_list_repaired)
    else:
        print("Function categorise_reference() returned something incompehensible.")
        return None

    return reference_object