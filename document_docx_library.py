import docx, re
#from docx.shared import Inches
from docx import Document
from docx.document import Document as _Document
from docx.oxml.text.paragraph import CT_P
from docx.oxml.table import CT_Tbl
from docx.table import _Cell, Table
from docx.text.paragraph import Paragraph
from docx.oxml.simpletypes import ST_Merge

import config

####DOCX LIBRARY BASED FUNCTIONS####BEGINNING####

#get document object via docx library
def get_document_by_docx_library():
    document = Document(config.filename)
    return document

#get a list of the article headings 
def get_headings():

    headings = []
    document = get_document_by_docx_library()

    for paragraph in document.paragraphs:
        if paragraph.style.name.startswith("Heading 1"):
            section_level = 1
            heading = paragraph.text
            headings.append([section_level, heading])
            
        elif paragraph.style.name.startswith("Heading 2"):
            section_level = 2
            heading = paragraph.text
            headings.append([section_level, heading])

        elif paragraph.style.name.startswith("Heading 3"):
            section_level = 3
            heading = paragraph.text
            headings.append([section_level, heading])

    return headings


#calculate the length of the document (max_index = document_length - 1)
def get_document_length():    #previous name->get max index
    document = get_document_by_docx_library()
    
    document_length = 0
    for block in iter_block_items(document):
        document_length += 1
    return document_length

#returns the element at a specific index
def get_element_by_index(index):
    document = get_document_by_docx_library()
    
    temp_index = 0
    for block in iter_block_items(document):
        if(temp_index == index):                        #be careful how to handle the return, it can be either text, image or table
            return block
        temp_index += 1
    return None  

#returns the index of a specific element 
#input is the whole paragraph or the beginning of it
def get_index_by_content(content):
    document = get_document_by_docx_library()

    index = 0
    for block in iter_block_items(document):
        match = re.search('^' + content +'(.+)?', block.text)
        if match:                             #TO ADD: appropriate exceptions for images, tables if necessary
            return index
        index += 1
    return None


#iterate document using docx library
def iter_block_items(parent):
    """
    Generate a reference to each paragraph and table child within *parent*,
    in document order. Each returned value is an instance of either Table or
    Paragraph. *parent* would most commonly be a reference to a main
    Document object, but also works for a _Cell object, which itself can
    contain paragraphs and tables.
    """
    if isinstance(parent, _Document):
        parent_elm = parent.element.body
        # print(parent_elm.xml)
    elif isinstance(parent, _Cell):
        parent_elm = parent._tc
    else:
        raise ValueError("something's not right")

    for child in parent_elm.iterchildren():
        if isinstance(child, CT_P):
            yield Paragraph(child, parent)
        elif isinstance(child, CT_Tbl):
            yield Table(child, parent)


#document length docx library based

"""
#gets the non empty paragraphs of the text and counts them
def get_full_text(filename):
    doc = docx.Document(filename)
    fullText = []

    #here i get the text in paragraphs
    paragraphCounter = 0
    for paragraph in doc.paragraphs:
        fullText.append(paragraph.text)
        
        strParagraph = str(paragraph.text)
        if paragraph.text != '':
            paragraphCounter += 1
            #print(paragraphCounter) 
            #print(paragraph.text) 
    return '\n'.join(fullText)
"""