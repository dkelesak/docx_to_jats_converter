import re
from authors import get_authors_dictionaries, create_authors_tree
from reference import Reference
from tree import Tree

#inherits from Reference class
class Journal_Article(Reference):
  def __init__(self, reference_id, certainty, title="", authors_dictionary_list=[], publication_year="", source="", volume="", issue="", fpage="", lpage=""):
    super().__init__(reference_id, certainty)
    self.title = title
    self.authors_dictionary_list = authors_dictionary_list                                  
    self.publication_year = publication_year
    self.source = source
    self.volume = volume
    self.issue = issue
    self.fpage = fpage      
    self.lpage = lpage                                

  #create tree of this reference
  def create_tree(self):
    reference_tree = Tree("ref", "", 0, ["id", "ref-" + str(self.reference_id)])
    
    reference_tree.construct_add_child("title", self.title)
    #create tree including the names of all the authors
    authors_tree = create_authors_tree(self.authors_dictionary_list)
    reference_tree.add_child_object(authors_tree)
    
    reference_tree.construct_add_child("year", self.publication_year)
    reference_tree.construct_add_child("source", self.source)
    reference_tree.construct_add_child("volume", self.volume)
    reference_tree.construct_add_child("issue", self.issue)
    reference_tree.construct_add_child("fpage", self.fpage)
    reference_tree.construct_add_child("lpage", self.lpage)

    return reference_tree


#takes a reference in the form of a list of 3 Reference_Sentence objects, analyses it and returns the corresponding Journal Article object
def create_journal_article(sentence_objects_list_repaired, reference_id):
    
    #first sentence
    authors_year_str = sentence_objects_list_repaired[0].sentence_str
    #split to authors and year
    year_match = re.compile(r'(\()?([1-2][0-9]{3}|3000)(\))?')
    if year_match:###repair this den elegxei kati
        match = year_match.search(authors_year_str)                       
        year_position = match.start()
        publication_year = match.group()
    authors_str = authors_year_str[:year_position]
    authors_dictionary_list = get_authors_dictionaries(authors_str)

    #second sentence
    title = sentence_objects_list_repaired[1].sentence_str

    #third sentence
    publisher_details_str = sentence_objects_list_repaired[2].sentence_str
    publisher_dictionary = interpret_journal_article_publisher_details(publisher_details_str)

    source = publisher_dictionary["source"]
    volume = publisher_dictionary["volume"]
    issue = publisher_dictionary["issue"]
    fpage = publisher_dictionary["fpage"]
    lpage = publisher_dictionary["lpage"]

    #create journal article object
    journal_article_reference = Journal_Article(reference_id, title, authors_dictionary_list, publication_year, source, volume, issue, fpage, lpage)  

    return journal_article_reference


#interpret publisher details of journal article reference
def interpret_journal_article_publisher_details(publisher_details_str):

    #temporary assignments 
    source = ""
    volume = "" 
    issue =  "" 
    fpage = "" 
    lpage = ""

    #work with numbers, catch specific surrounding characters(even if there is space between)
    #match_number_and_surrounding_character = re.compile(r'\(? ?\d+ ?[)-]?')                   

    #what if no numbers case?
    #match all numbers
    match_numbers = re.compile(r'\d+')
    matching_numbers_list = match_numbers.findall(publisher_details_str)
    #number_of_matching_numbers = len(matching_numbers_list)

    #match volume - suppose first number corresponds to volume
    match_volume = match_numbers.search(publisher_details_str)
    if match_volume:
        #get source -> it it just before the first number
        index_of_first_number = match_volume.start()    #or match_numbers.search(publisher).start() ??
        source = str(publisher_details_str[:index_of_first_number])
        volume = match_volume.group()
    #case there are no numbers
    else:
        source = publisher_details_str   

    #match issue
    match_issue = re.search(r'\( ?(\d+) ?\)', publisher_details_str)
    if match_issue:
        issue = match_issue.group().strip("(").strip(")").strip()  

    #match pages
    match_pages = re.search(r'(\d+) ?\- ?(\d+)', publisher_details_str)
    if match_pages:
        pages_str = match_pages.group() 
        pages_split = pages_str.split("-")
        fpage = pages_split[0].strip()
        lpage = pages_split[1].strip()

    publisher_dictionary = {"source": source, "volume": volume, "issue": issue, "fpage": fpage, "lpage": lpage}
    
    return publisher_dictionary