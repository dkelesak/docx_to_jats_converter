from datetime import datetime
import re
from authors import get_authors_dictionaries, create_authors_tree
from dates import get_date, convert_date_to_iso8601_str
from reference import Reference
from tree import Tree

#inherits from Reference class
class Online_Reference(Reference):

  def __init__(self, reference_id, certainty, authors_dictionary_list=[], publication_year="", source="", url="", date_accessed=""):
    super().__init__(reference_id, certainty)
    self.authors_dictionary_list = authors_dictionary_list                                    
    self.publication_year = publication_year
    self.source = source
    self.url = url
    self.date_accessed = date_accessed
  
  #create tree of this reference
  def create_tree(self):
    reference_tree = Tree("ref", "", 0, ["id", "ref-" + str(self.reference_id)])
    
    #create tree including the names of all the authors
    authors_tree = create_authors_tree(self.authors_dictionary_list)
    reference_tree.add_child_object(authors_tree)

    reference_tree.construct_add_child("year", self.publication_year)
    reference_tree.construct_add_child("source", self.source)
    reference_tree.construct_add_child("uri", self.url)
    reference_tree.construct_add_child("date_accessed", self.date_accessed)

    return reference_tree


#analyse reference string of online reference        
def create_online_reference(sentence_objects_list_repaired, reference_id):

  author_dictionary_list = []
  publication_year = ""
  source = ""
  url = ""
  date_accessed = ""

  #source is included at sentence 2 after sentence tokenisation repair
  source = sentence_objects_list_repaired[1].reference_str

  #url
  url_check = re.compile(r'(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:/[^\s]*)?') 

  for i, reference_sentence_object in enumerate(sentence_objects_list_repaired):
      url_match = url_check.search(reference_sentence_object.sentence_str)
      if url_match:
          url = url_match.group()
          break

  #i has the index of url sentence
  index_url_sentence = i

  #date_accessed
  #expected case based on sentence tokenisation -> url at sentence #3
  #check to find the date when the url was accessed, starting from the last sentence(sentence #3) as it is where it is expected to be
  #this helps avoid confusion in case date of publishing and date accessed are both included in the initial reference string
  for reference_sentence_object in reversed(sentence_objects_list_repaired):
      date = get_date(reference_sentence_object.sentence_str)
      if isinstance(date, datetime):
          break
  
  #if found date convert it to iso8601 format
  if isinstance(date, datetime):
      date_accessed = convert_date_to_iso8601_str(date)
  
  """
  #authors/organisation appear usually before publication year
  """
  online_reference = Online_Reference(reference_id, author_dictionary_list, publication_year, source, url, date_accessed)

  return online_reference