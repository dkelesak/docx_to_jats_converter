import re
from tree import Tree
from document_docx2python_library import get_not_blank_paragraphs_with_formatting, get_not_blank_paragraphs_without_formatting
from document_docx_library import get_headings
from endnotesfootnotes import get_endnotes_footnotes_combined_list
from lists import get_list_object, create_list_tree
from figures import get_figure_objects_list, create_figure_tree
from tables import get_table_objects_list, create_table_tree


#create a tree corresponding to the body (sections and paragraphs)
def create_body_tree(body_beginning, body_end):
    index = 0
    current_section_level = 0
    initial_tags_str = ""

    #beginning and end variables are used to go just after the figure/table (because docx and docx2python have differerent method of indexing)
    #table variables
    table_beginning = -1
    table_end = -1

    #figure variables
    figure_beginning = -1
    figure_end = -1

    #list variables
    list_index = 0             #no of list
    list_beginning = -1
    list_end = -1

    #list of tables
    tables_list = get_table_objects_list()
    
    #list of figures
    figures_list = get_figure_objects_list()

    #endnotes, footnotes combined index
    endnote_footnote_combined_index = 0

    #create root
    body_tree = Tree("body")

    found_chapter_numbering_auto_headings = False
    found_chapter_numbering_auto_list = False
    found_chapter_numbering_manual = False
    found_chapter_numbering_styling = False

    for paragraph in get_not_blank_paragraphs_with_formatting()[body_beginning:body_end+1]:
        if index not in range(table_beginning, table_end) and index not in range(list_beginning, list_end) and index not in range(figure_beginning, figure_end): 

            #check if it chapter title of any format and any level                                                              
            found_chapter_numbering_auto_headings = False
            headings = get_headings()
            for heading in headings:
                if heading[1] == paragraph.strip():
                    found_chapter_numbering_auto_headings = True
                    break
        
            found_initial_tags = re.search(r'(<.+?>){1,}', paragraph)
            if found_initial_tags:
                initial_tags_str = found_initial_tags.group()
                paragraph = paragraph[found_initial_tags.end():]

            found_chapter_numbering_auto_list = re.search(r'^([\t]?[\t]?[1-9]\)\t)', paragraph)            
            found_chapter_numbering_manual = re.search(r'^([1-9]\.)+[\d]?', paragraph)        
          

            #if paragraph corresponds to chapter title
            if found_chapter_numbering_auto_headings or found_chapter_numbering_auto_list or found_chapter_numbering_manual:
                
                if found_chapter_numbering_auto_headings:
                    for heading in headings:
                        if heading[1] == paragraph.strip():
                            current_section_level = heading[0]
                            section_title = heading[1] 
                            break

                elif found_chapter_numbering_auto_list:            
                
                    exact_section = str(found_chapter_numbering_auto_list.group()) #.split('\b'))
                    validated_exact_section = []

                    current_section_level = exact_section.count('\t')

                    section_number_check = re.search(r'\d+', exact_section)
                    section_number = section_number_check.group()
                    
                    section_title_draft = paragraph.split('\t') 
                    section_title = initial_tags_str + section_title_draft[len(section_title_draft)-1]

                elif found_chapter_numbering_manual:
                    #find level of section (e.g. if it is subsection of subsection etc.)
                    
                    validated_exact_section = []
                    exact_section = re.search(r'^([1-9]\.)+[\d+]?\.?[\d+]?\.?', paragraph)  

                    exact_section_split = exact_section.group().split(".")            
                    
                    current_section_level = 0
                    #calculate section level
                    for subsection_number in exact_section_split:
                        if subsection_number.isdigit():
                            current_section_level += 1

                    #section title is just after the section number
                    section_title = initial_tags_str + paragraph[exact_section.end()+1:].strip()


                #create section node and add to tree
                section_node = Tree("sec", "", current_section_level)                         
                section_node.construct_add_child("title", section_title)

                #currently works for sections up to level 3
                #add to body_tree
                if current_section_level == 1:
                    body_tree.add_child_object(section_node)
                elif current_section_level == 2:
                    body_tree.children[-1].add_child_object(section_node)           
                elif current_section_level == 3:
                    body_tree.children[-1].children[-1].add_child_object(section_node)


            #if not chapter title with numbering
            else:
                #Note: it is supposed figure/table captions are before the figure/table

                #check if figure caption
                figure = find_figure(index)
                #check if table caption
                table_index = find_table(paragraph)
                #check if beginning of list 
                found_list = re.search(r'^--\t', paragraph) 

                #check if there is the possibility to be a not numbered section title
                found_chapter_styling = re.search( r'^<bold>.*?</bold>$', paragraph)
                
                #check if endnote or footnote and replace it with corresponding text and accompanying tags
                found_endnote_or_footnote = re.search(r'----endnote+[\d+]+----|----footnote+[\d+]+----', paragraph)
                if found_endnote_or_footnote:
                    paragraph = re.sub(r'----endnote+[\d+]+----|----footnote+[\d+]+----', get_endnotes_footnotes_combined_list()[endnote_footnote_combined_index], paragraph)
                    endnote_footnote_combined_index += 1


                if figure is not None:
                    #figure = figures_list[figure_index]   
                    figure_tree = create_figure_tree(figure)
                    section_node.add_child_object(figure_tree)

                    figure_beginning = index
                    figure_end = index + 2
                
                elif table_index is not None:
                    table = tables_list[table_index] 
                    table_tree = create_table_tree(table)
                    section_node.add_child_object(table_tree)
                    
                    #calculate table dimensions
                    number_of_rows = len(table.table_data)
                    number_of_columns = len(table.table_data[0])

                    #calculate starting and ending index of table
                    #current index corresponds to the caption
                    table_beginning = index + 1
                    table_end = table_beginning + number_of_rows * number_of_columns - 1

                elif found_list:
                    list_object = get_list_object(index, list_index)
                    list_tree = create_list_tree(list_object)
                    section_node.add_child_object(list_tree)

                    list_beginning = index
                    list_end = index + len(list_object.list_items)
                    list_index += 1
                
                #if it is not a title of a numbered section and if it does not belong to figure/table/list
                #and if the meantime this paragraph is bold from beginning to end then it is a not numbered section title
                elif found_chapter_styling:

                    current_section_level += 1     #BETTER FIX HERE
                    #THELO NA PIANEI KAI SE MIKTA SYSTHMATA SECTION

                    section_title = found_chapter_styling.group()
              
                    #create section node and add to tree
                    section_node = Tree("sec", "", current_section_level)                         
                    section_node.construct_add_child("title", section_title)

                    #currently works for sections up to level 3
                    #add to body_tree
                    if current_section_level == 1:
                        body_tree.add_child_object(section_node)
                    elif current_section_level == 2:
                        body_tree.children[-1].add_child_object(section_node)           
                    elif current_section_level == 3:
                        body_tree.children[-1].children[-1].add_child_object(section_node)
                
                #catch corner case of not find chapter title of first chapter in any format
                elif index == body_beginning:
                    print("Unable to find chapter 1 title. Either it does not exist or it is a bug.")

                #else text paragraph         
                else:
                    #get text paragraph with formatting tags 
                    paragraph = get_not_blank_paragraphs_with_formatting()[index]                                                       
                    section_node.construct_add_child("p", paragraph)

        index += 1

    return body_tree


#find beginning of body by finding title of 1st chapter
def find_body_beginning(abstract_beginning):
    index = 0

    for paragraph in get_not_blank_paragraphs_without_formatting():
        if index > abstract_beginning:

            found_chapter_numbering_auto_headings = False
            headings = get_headings()
            for heading in headings:
                if heading[1] == paragraph.strip():
                    found_chapter_numbering_auto_headings = True
                    break
        
            found_chapter_numbering_auto_list = re.search(r'^([\t]?[\t]?[1-9]\)\t)', paragraph)            
            found_chapter_numbering_manual = re.search(r'^([1-9]\.)+[\d]?', paragraph)        
            found_chapter_styling = re.search( r'^<bold>.*?</bold>$', paragraph)

            if found_chapter_numbering_auto_headings or found_chapter_numbering_auto_list or found_chapter_numbering_manual or found_chapter_styling:
                return index
        index += 1

    return None


#check if paragraph corresponds to figure caption, and if so return corresponding figure object
def find_figure(index):
    figure_objects_list = get_figure_objects_list()
    for figure in figure_objects_list:
        #paragraph_index-1 -> corresponds to index of caption
        if index == figure.paragraph_index - 1: 
            return figure
    return None

###ADJUST THIS FUNCTIONS OPWS THN APO PANW
#check if paragraph corresponds to table function, and if so return corresponding table object
def find_table(index):
    table_objects_list = get_table_objects_list()
    for table in table_objects_list:
        #paragraph_index-1 -> corresponds to index of caption
        if index == table.paragraph_index - 1: 
            return table
    return None